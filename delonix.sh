#!/bin/bash
bang=`head -2 $1 | grep "#!"`
topline=`head -2 $1 | grep -v "#!" | head -1 | tr "\r\n" " "`
output=$(bash -c "$topline" 2>&1)
echo -e "${bang}\n${topline}\n$output"
