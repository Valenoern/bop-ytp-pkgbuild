#!/bin/bash

linkname=$1
linkdest=`readlink $linkname`

rm "$linkname"
cp -a "$linkdest" "$linkname"

# cr. 2024-01-06 T00:15:23Z
