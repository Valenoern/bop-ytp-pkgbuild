#!/bin/bash

# caution: I am not sure if this script is properly secure
# I was able to separately run sudo and then run this script through a privilege elevation,
# which seems like a problem. I will look into polkit

user=`whoami`
if [[ -f /home/user ]]; then
  sudo rm -f /home/user
fi
pushd /home
sudo ln -s ./$user /home/user

# cr. 2024-01-01 T02:27:42Z
