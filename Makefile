# v-refracta prototype packages 2024 experiment
# pkgrko = ... packaged
# pkgintrepid = ... packaged
# pkgirritator = ?

top:
	echo "This does nothing"

archaeo:
	vrfx-core archaeopteryx
	vrfx-aur  archaeopteryx --db

intrepid:
	vrfx-core intrepidus
	vrfx-aur  intrepidus --db


# to toggle pacman configurations:                    vrfx-build   vrfx-install
# to list packages in a specific suite roster file:   vrfx-packages
# to update suite cache:                              vrfx-suites
# to build suite X:                                   vrfx-core X  vrfx-aur X  [--db]


# in order to clean all the packages
clean:
	echo "This does nothing"
	#
	#pushd sbcli; make clean; popd
	#pushd runlisp; make clean; popd
	#pushd pkgbend; make clean; popd
	#
	#pushd cl-ppcre; make clean; popd
	#pushd cl-puri; make clean; popd
	#pushd cl-osicat; make clean; popd
	#pushd cl-html-template; make clean; popd
	#pushd cl-closure-common; make clean; popd
	#pushd cl-cxml; make clean; popd
	#pushd cl-rdfxml; make clean; popd
	#
	#pushd cl-grevillea; make clean; popd
	#pushd cl-xylem; make clean; popd
	#pushd jsown; make clean; popd
	#pushd ytp; make clean; popd
	#pushd bop-owlwings; make clean; popd
	#
	#pushd bop-links; make clean; popd
	#pushd bop; make clean; popd
	#pushd bop-mini; make clean; popd
	#pushd bop-docs; make clean; popd
	#
	#pushd v-rfx-i3; make clean; popd
	#pushd v-rfx-bluetooth; make clean; popd
	#pushd v-rfx-coding; make clean; popd
	#pushd v-rfx-browse; make clean; popd
	#pushd v-rfx-server; make clean; popd
	#pushd kate-mousepad; make clean; popd
	#
	#pushd vhash; make clean; popd
	#pushd v-abstract; make clean; popd
	#pushd v-calculate; make clean; popd
