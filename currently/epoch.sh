#!/bin/bash

if [[ -n "$1" ]];
 then
  date +%s --date $1
 else
  date +%s
fi
