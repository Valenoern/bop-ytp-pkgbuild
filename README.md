These are shell scripts to build arch/manjaro packages.

https://wiki.archlinux.org/title/PKGBUILD

They are not necessarily ready for the public; if you try to install them there may be some missing dependencies, etc.

eventually, the bopwiki source repository will be able to install everything needed for bop from its `make install` command.
