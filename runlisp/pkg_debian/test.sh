#!/bin/bash

export CONTENTS_PATH="./pkg_debian"
export PACKAGE_PATH="bop-runlisp_2022*_all.deb"

# run test using pkgbend
/usr/share/pkgbend/deb_test.sh
