# Maintainer: Valenoern <valenoern@distributary>
# Contributor: Brian Bidulock <bidulock@openss7.org>
# Contributor: Juan Diego Tascon
# Contributor: Swift Geek
pkgname=bluez-git
_pkgname=bluez
pkgver=5.66
  # note: this version was selected manually due to bugs that appeared somewhere around 5.72
pkgrel=2
epoch=1
pkgdesc="Libraries and tools for the Bluetooth protocol stack"

url="http://www.bluez.org/"
  # PKGBUILD source: https://aur.archlinux.org/cgit/aur.git/commit/?h=bluez-git&id=92c4a21e982310d664f1030aae481ee93b30bb6e
arch=('i686' 'x86_64' 'aarch64')
license=('GPL2')
depends=('libical' 'libdbus' 'glib2' 'alsa-lib' 'ell' 'json-c')
optdepends=('cups: CUPS backend')
makedepends=('git' 'python-docutils')
conflicts=($_pkgname
	   $_pkgname-utils
	   $_pkgname-libs
	   $_pkgname-cups
	   $_pkgname-hid2hci
	   $_pkgname-plugins
	   $_pkgname-hcidump
           'obexd-client'
	   'obexd-server')
provides=(libbluetooth.so
	  $_pkgname=${pkgver%%.r*}-${pkgrel}
          $_pkgname-utils=${pkgver%%.r*}-${pkgrel}
	  $_pkgname-libs=${pkgver%%.r*}-${pkgrel}
	  $_pkgname-cups=${pkgver%%.r*}-${pkgrel}
	  $_pkgname-hid2hci=${pkgver%%.r*}-${pkgrel}
	  $_pkgname-plugins=${pkgver%%.r*}-${pkgrel})
backup=('etc/bluetooth/main.conf')
source=("$pkgname::git://git.kernel.org/pub/scm/bluetooth/bluez.git#commit=63b01d0b0661d6a0048107dcb9e6f12ceac409e0"
	bluetooth.modprobe)
md5sums=('SKIP'
         '671c15e99d7154c2df987b71c5851b3d')

#pkgver() {
#  cd $pkgname
#  git describe --tags --long|sed -r 's,^[^0-9],,;s,([0-9]*-g),r\1,;s,[-_],.,g'
#}

prepare() {
  cd $pkgname
  ./bootstrap
}

build() {
  cd $pkgname
  ./configure \
    --prefix=/usr \
    --mandir=/usr/share/man \
    --sysconfdir=/etc \
    --localstatedir=/var \
    --libexecdir=/usr/lib \
    --with-dbusconfdir=/usr/share \
    --enable-external-ell \
    --enable-btpclient \
    --enable-midi \
    --enable-sixaxis \
    --enable-mesh \
    --enable-hid2hci \
    --enable-experimental \
    --enable-manpages \
    --enable-library # this is deprecated
  make
}
  
check() {
  cd $pkgname
  # tests segfault and hang
#  make check || /bin/true # https://bugzilla.kernel.org/show_bug.cgi?id=196621
}


package() {
  cd $pkgname
  make DESTDIR="${pkgdir}" install
  # add missing toosl FS#41132, FS#41687, FS#42716
  for f in `find tools/ -type f -perm -755`; do
    install -Dm755 "$f" "${pkgdir}/usr/bin/$(basename $f)"
  done
  install -Dm644 src/main.conf        "${pkgdir}/etc/bluetooth/main.conf"
  install -Dm644 ${srcdir}/bluetooth.modprobe "${pkgdir}/usr/lib/modprobe.d/bluetooth-usb.conf"
}

# vim:set ts=2 sw=2 et:
