#!/bin/bash

parent=/opt/v-refracta
arg=$2
repodir=$1
repo=`basename $repodir`
pkgdb=`readlink -f ${parent}/pkg-db`
shift
packages=`vrfx-packages $repo core`

sudo pacman -Syw --cachedir ${parent}/${repo} --dbpath $pkgdb  $packages

if [[ "$arg" = "--db" ]]; then
 vrfx-db $repo
fi
