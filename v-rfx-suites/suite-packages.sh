#!/bin/bash

usr=/usr/share/v-refracta/suites
etc=/etc/v-refracta/suites
var=/var/cache/v-refracta/suites
repo=$1
quality=$2

# if repo exists as a folder, select the specified file from it
if [[ -d ${repo} ]]; then
  repo=${repo}/${quality}
 # if repo exists as a file, select the file
 elif [[ -f ${repo} ]]; then
  repo=${repo}
 # if a simple repo name was given, assume repo is in /var/cache
 elif [[ -f ${var}/${repo}/${quality} ]]; then
  repo=${var}/${repo}/${quality}
 # if the repo was not cached then try /etc and /usr
 elif [[ -f ${etc}/${repo}/${quality} ]]; then
  repo=${etc}/${repo}/${quality}
 elif [[ -f ${usr}/${repo}/${quality} ]]; then
  repo=${usr}/${repo}/${quality}
 # if repo could not be found anywhere, quit
 else
  echo "Repository ${repo} not found"
  exit 2
fi

cat $repo | grep "#" -v | tr "\n" " "
