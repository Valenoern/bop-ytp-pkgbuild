#!/bin/bash

parent=/opt/v-refracta
repodir=`readlink -f $1`
repo=`basename $repodir`
pkgdb=`readlink -f ${parent}/pkg-db`
shift
packages=$@

repo-add -n ${parent}/${repo}/${repo}.db.tar.gz ${parent}/${repo}/*.pkg.tar.xz ${parent}/${repo}/*.pkg.tar.zst
